Steerpath Smart Campus

# Change Log

All app release notes for Android and iOS applications will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.1.1] - 2021-01-25

- Map updates
- Improved back navigation

## [3.1.0] - 2020-10-12

### Fixed

- Sharing GPS location to other users
- Live user title is visible on the map

## [3.0.1] - 2020-05-18

### Fixed

- Fixed crash issue with floor picker (Android)

## [3.0.0] - 2020-04-30

Initial release of new Steerpath Smart Campus app that replaces old Steerpath Demo App (Android) in PlayStore and Steerpath (iOS) in AppStore.
