/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {useSteerpath} from '../hooks/useSteerpath';

// We abstract away the actual context to prevent accidental misuse
const SteerpathContext = React.createContext<
  ReturnType<typeof useSteerpath> | undefined
>(undefined);

export function SteerpathProvider({children}: {children: React.ReactNode}) {
  const steerpathData = useSteerpath();
  return (
    <SteerpathContext.Provider value={steerpathData}>
      {children}
    </SteerpathContext.Provider>
  );
}

export const useSteerpathContext = () => {
  const context = React.useContext(SteerpathContext);

  if (!context) {
    throw new Error('Steerpath context hook is not used correctly');
  }
  return context;
};
