/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import Button, {ButtonProps} from './Button';
import {colors} from '../../styles/colors';

interface CheckButtonProps {
  isSelected: boolean;
}

const CheckButton: React.FC<CheckButtonProps & ButtonProps> = ({
  isSelected,
  ...props
}) => {
  const [isButtonSelected, setIsButtonSelected] = useState(isSelected);
  useEffect(() => {
    setIsButtonSelected(isSelected);
  }, [isSelected]);

  const unCheckIcon = () => {
    return (
      <View
        style={{
          width: 26,
          height: 26,
          borderRadius: 13,
          backgroundColor: colors.uncheckedColor,
        }}
      />
    );
  };

  return (
    <Button
      iconComponent={
        isButtonSelected ? (
          <Icon name="check-circle" size={25} color={colors.brand_color} />
        ) : (
          unCheckIcon()
        )
      }
      isWhiteText={false}
      {...props}
    />
  );
};

export default CheckButton;
