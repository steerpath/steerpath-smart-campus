/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {StyleSheet, Text, TextProps} from 'react-native';
import {colors} from '../../styles/colors';
import {typography} from '../../styles/typography';

type Variant =
  | 'h1'
  | 'h2'
  | 'h3'
  | 'h6'
  | 'body1'
  | 'body2'
  | 'button'
  | 'tiny';
type TypographyColors =
  | 'brand_color'
  | 'brand_content_on_brand'
  | 'card_background'
  | 'text'
  | 'background'
  | 'link'
  | 'disabled_link'
  | 'unimportant'
  | 'button_background'
  | 'grey'
  | 'card_outline';

interface TypographyProps extends TextProps {
  variant: Variant;
  color?: TypographyColors;
  children: React.ReactNode;
  bold?: boolean;
}

const Typography: React.FC<TypographyProps> = ({
  style = {},
  variant,
  children,
  color = colors.text,
  ...otherProps
}) => {
  return (
    <Text
      {...otherProps}
      style={[
        defaultStyles.default,
        defaultStyles[variant],
        {color: colors[color]},
        style,
      ]}>
      {children}
    </Text>
  );
};

export default Typography;

// Todo: Add font family
const defaultStyles = StyleSheet.create(typography as any);
