/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Avatar} from 'react-native-elements';
import CardRow from './CardRow';
import Typography from '../typography/Typography';
import {colors} from '../../styles/colors';
import CheckButton from '../button/CheckButton';
import {Effects} from '../../styles';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {useI18nContext} from '../../context/I18nContext';

interface ProfileCardProps {
  onCheckedChanged: (boolean) => void;
  isVisible: boolean;
  userTitle: string;
  editPressed: () => void;
}

const ProfileCardContainer: React.FC<ProfileCardProps> = ({
  onCheckedChanged,
  isVisible,
  userTitle,
  editPressed = () => {},
}) => {
  const {I18n} = useI18nContext();

  return (
    <View style={styles.container}>
      <CardRow styles={{alignItems: 'center'}}>
        {userTitle ? (
          <TouchableOpacity
            style={{
              position: 'absolute',
              right: 0,
              top: 0,
              backgroundColor: colors.brand_content_on_brand,
              borderRadius: 10,
            }}
            onPress={editPressed}>
            <Icon name="pencil-outline" size={20} color={colors.brand_color} />
          </TouchableOpacity>
        ) : null}

        <View style={{position: 'relative'}}>
          <Avatar
            size={100}
            rounded
            source={require('../../../assets/images/avatar_default.png')}
          />
          {/* <View style={{position: 'absolute', right: 0}}>
            <Image
              source={require('../../../assets/images/statusIcon.png')}
              style={{height: 30, width: 30, resizeMode: 'cover'}}
            />
          </View> */}
        </View>
        <View style={{marginLeft: 30, flex: 1}}>
          <Typography variant="h1" bold>
            {userTitle && userTitle !== '' ? userTitle : 'Anonymous'}
          </Typography>
        </View>
      </CardRow>
      {/* <View style={{marginVertical: 10}}>
        <LongButtonWithAnimation
          buttons={[
            {title: 'away', onClick: () => {}},
            {title: 'free', onClick: () => {}},
            {title: 'busy', onClick: () => {}},
          ]}
        />
      </View> */}
      <CardRow
        styles={{
          marginVertical: 10,
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <CardRow styles={{alignItems: 'center', marginRight: 15}}>
          {/* <Icon name="map-marker" size={20} color={colors.grey} />
          <View style={{marginLeft: 10}}>
            <Typography variant="body1">D-wing, T-building</Typography>
          </View> */}
        </CardRow>
        <View style={{flex: 1}}>
          <CheckButton
            buttonText={I18n.t('optionScreen.profileCard.visibility')}
            onPress={() => {
              onCheckedChanged(!isVisible);
            }}
            isSelected={isVisible}
            isWhiteText={false}
          />
        </View>
      </CardRow>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginVertical: 10,
    backgroundColor: colors.card_background,
    borderRadius: 5,
    ...Effects.shadow,
  },
  marginText: {
    marginTop: 10,
  },
});

export default ProfileCardContainer;
