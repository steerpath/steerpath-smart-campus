/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const black = '#000000';
export const dirtyWhite = '#fcfcfc';
export const darkestGray = '#29292c';
export const darkGray = '#3d3e44';
export const mediumGray = '#9b9aa1';
export const lightGray = '#eeeeee';
export const lightWarmGray = '#f5f5f5';
export const offWhite = '#b7bdc5';
export const tan = '#fcf8f2';
export const thoughtbotRed = '#ed3e44';
export const transparent = 'rgba(0, 0, 0, 0)';
export const spaceGray = '#333';
export const error = '#ef5350';
export const succcess = '#66bb6a';

// app colors
// export const purpleLight = '#be8bff';
export const purple = '#be8bff';
// export const purpleDark = '#7e33ff';
// senaatti purple
export const purpleDark = '#87189d';
export const purpleLight = '#c38bce';
export const gray = '#aaaaaa';
export const grayDark = '#999999';
export const darkSlateBlue = '#273493';
export const darkRed = '#ab0801';
export const white = '#ffffff';
export const linkBlue = '#3af';

export const whiteTransparent = '#ffffff00';

export const red = '#ba4d38';

export const baseText = darkGray;
export const darkText = darkestGray;
export const sectionBackground = lightWarmGray;
export const background = white;
export const selected = thoughtbotRed;
export const unselected = lightGray;

export interface Colors {
  brand_color: string;
  brand_content_on_brand: string;
  card_background: string;
  text: string;
  background: string;
  link: string;
  disabled_link: string;
  unimportant: string;
  button_background: string;
  card_outline: string;
  grey: string;
  lightGrey: string;
  red: string;
  uncheckedColor: string;
  white: string;
  inputBackground: string;
  black: string;
  error: string;
}

export const colors: Colors = {
  brand_color: '#2E7B80',
  brand_content_on_brand: '#fff',
  card_background: '#fff',
  text: '#444',
  background: '#F7F7F7',
  link: '#555555',
  disabled_link: '#CCCCCC',
  unimportant: '#7A7A7A',
  button_background: '#F5F5F5',
  card_outline: '#EEEEEE',
  grey: '#DADADA',
  lightGrey: '#F8F8F8',
  red: '#DA2D2D',
  uncheckedColor: '#DBDBDB',
  white: '#ffffff',
  inputBackground: '#6da2a5',
  black: '#000000',
  error: '#ef5350',
};
