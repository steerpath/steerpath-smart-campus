import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {View} from 'react-native';
import {MapStack} from './MapStack';
import SetCodeScreen from '../screens/SetCodeScreen';
import Spinner from '../components/Spinner';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useSteerpathContext} from '../context/SteerpathContext';

const AppStack = createStackNavigator();

export function RootNavigator() {
  const insets = useSafeAreaInsets();
  const {initializing, sdkReady} = useSteerpathContext();
  return (
    <View style={{flex: 1, marginBottom: insets ? insets.bottom : 0}}>
      <NavigationContainer>
        {!initializing ? (
          <AppStack.Navigator headerMode="none">
            {sdkReady ? (
              // The name is "MapStack", not "Map" to prevent confusion with the actual map screen
              <AppStack.Screen name="MapStack" component={MapStack} />
            ) : (
              <AppStack.Screen name="Set Code" component={SetCodeScreen} />
            )}
          </AppStack.Navigator>
        ) : (
          <Spinner />
        )}
      </NavigationContainer>
    </View>
  );
}
