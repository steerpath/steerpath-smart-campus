/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';

import {colors} from '../styles/colors';

import OptionsScreen from '../screens/OptionsScreen';
import AboutScreen from '../screens/AboutScreen';

import {createStackNavigator} from '@react-navigation/stack';
import {useSteerpathContext} from '../context/SteerpathContext';

const Stack = createStackNavigator();
export function OptionStack() {
  const {usesLive} = useSteerpathContext();
  return (
    <Stack.Navigator
      initialRouteName="Options"
      screenOptions={{
        headerTintColor: colors.white,
        headerStyle: {
          backgroundColor: colors.brand_color,
        },
        headerTitleStyle: {
          color: colors.white,
          alignSelf: 'center',
          textAlign: 'center',
          justifyContent: 'center',
          flex: 1,
          textAlignVertical: 'center',
        },
      }}>
      <Stack.Screen
        name="Options"
        component={OptionsScreen}
        options={{
          headerShown: usesLive ? false : true,
        }}
      />
      <Stack.Screen name="About" component={AboutScreen} />
    </Stack.Navigator>
  );
}
