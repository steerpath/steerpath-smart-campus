/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import MapScreen from '../screens/MapScreen';
import {colors} from '../styles/colors';
import {typography} from '../styles/typography';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {OptionStack} from './OptionStack';
import {useI18nContext} from '../context/I18nContext';
const Tabs = createBottomTabNavigator();
export function MapStack() {
  const {I18n} = useI18nContext();

  return (
    <Tabs.Navigator
      initialRouteName="Map"
      tabBarOptions={{
        activeTintColor: colors.brand_color,
        inactiveTintColor: colors.brand_color,
        showLabel: true,
        labelStyle: {
          fontSize: typography.body1.fontSize,
        },
        style: {
          backgroundColor: colors.card_background,
          height: 65,
          elevation: 5,
          borderTopWidth: 0,
          paddingBottom: 0,
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          let iconName;
          switch (route.name) {
            case I18n.t('appTabs.map'):
              iconName = 'map';
              break;
            case I18n.t('appTabs.option'):
              iconName = 'cogs';
              break;
          }

          // You can return any component that you like here!
          return (
            <View
              style={
                [
                  {
                    width: '100%',
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingTop: 10,
                  },
                  focused
                    ? {borderTopColor: colors.brand_color, borderTopWidth: 2}
                    : {},
                ] as any
              }>
              <Icon name={iconName} size={26} color={colors.brand_color} />
            </View>
          );
        },
      })}>
      <Tabs.Screen name={I18n.t('appTabs.map')} component={MapScreen} />
      <Tabs.Screen name={I18n.t('appTabs.option')} component={OptionStack} />
    </Tabs.Navigator>
  );
}
