/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {PreferenceList} from '../components/preferences/PreferenceList';
import {useSteerpathContext} from '../context/SteerpathContext';
import {PreferenceListItem} from '../components/preferences/PreferenceItem';
import {baseFontSize} from '../styles/typography';
import {colors, darkGray, gray} from '../styles/colors';
import Text from '../components/typography/Typography';
import ProfileCardContainer from '../components/cards/ProfileCardContainer';
import ColoredStatusBar from '../components/status-bar/ColoredStatusBar';
import Dialog from 'react-native-dialog';
import {UserState} from '../hooks/types';
import {useI18nContext} from '../context/I18nContext';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default function OptionsScreen({navigation}) {
  const {clearSavedData} = useSteerpathContext();
  const [profileCardHeight, setProfileCardHeight] = useState<number>(0);
  const [showDialog, setShowDialog] = useState(false);
  const [userTitle, setUserTitle] = useState<string>('');
  const [disabled, setDisabled] = useState(true);
  const {shareLocation, userData, updateUserData, usesLive} = useSteerpathContext();
  const [isVisible, setIsVisible] = useState(false);
  const {I18n} = useI18nContext();

  useEffect(() => {
    if (userData) {
      setIsVisible(userData.shareLocation);
      setUserTitle(userData.title);
    }
  }, [userData]);

  const onCheckedChanged = (enabled: boolean) => {
    function setUserData() {
      if (userData) {
        (userData as UserState).shareLocation = enabled;
        updateUserData(userData);
      }
    }

    if (!enabled) {
      shareLocation(false);
      setShowDialog(false);
      setIsVisible(false);
      setUserData();
    } else {
      if (userTitle) {
        setShowDialog(false);
        shareLocation(true);
        setIsVisible(true);
        setUserData();
      } else {
        setShowDialog(true);
      }
    }
  };

  useEffect(() => {
    if (!showDialog) {
      setDisabled(true);
    }
  }, [showDialog]);

  const handleConfirm = () => {
    if (userData) {
      (userData as UserState).title = userTitle;
      (userData as UserState).shareLocation = true;
      updateUserData(userData);
      setShowDialog(false);
      shareLocation(true, userTitle);
      setIsVisible(true);
    } else {
      // TODO: show error
      setShowDialog(false);
    }
  };

  const handleCancel = () => {
    setShowDialog(false);
    const userTitle = userData && userData.title ? userData.title : '';
    setUserTitle(userTitle);
    setIsVisible(userData?.shareLocation ?? false);
  };

  return (
    <>
      {usesLive ? <ColoredStatusBar /> : null}
      <KeyboardAwareScrollView bounces={false} keyboardShouldPersistTaps={'handled'}>
        {usesLive ? (
          <View style={{flex: 1}}>
            <View style={styles.profileHeader}>
              <View
                style={{
                  height: 60,
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                  justifyContent: 'space-between',
                  backgroundColor: colors.brand_color,
                }}>
                <View style={{flex: 1, alignItems: 'center'}}>
                  <Text bold variant="h3" color="brand_content_on_brand">
                    {I18n.t('optionScreen.title')}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                backgroundColor: colors.brand_color,
                width: '100%',
                height: profileCardHeight / 2,
              }}
            />
            <View
              style={{
                marginTop: -profileCardHeight / 2,
                marginHorizontal: 20,
              }}
              onLayout={(e) => {
                setProfileCardHeight(e.nativeEvent.layout.height);
              }}>
              <ProfileCardContainer
                onCheckedChanged={onCheckedChanged}
                isVisible={isVisible}
                userTitle={userData ? userData.title : ''}
                editPressed={() => {
                  setShowDialog(true);
                }}
              />
            </View>
          </View>
        ) : null}

        <PreferenceList sectionTitle={I18n.t('optionScreen.preferences')}>
          <PreferenceListItem
            preferenceIconName="information-variant"
            preferenceTitle={I18n.t('optionScreen.aboutBtnTitle')}
            preferenceDescription={I18n.t('optionScreen.aboutBtnDescription')}
            onPress={() => {
              navigation.navigate('About');
            }}
          />
          <PreferenceListItem
            preferenceIconName="logout"
            preferenceTitle={I18n.t('optionScreen.venueCodeBtnTitle')}
            preferenceDescription={I18n.t('optionScreen.venueCodeBtnDescription')}
            onPress={clearSavedData}
          />
        </PreferenceList>
        <Dialog.Container visible={showDialog}>
          <Dialog.Title>
            {userData && userData.title
              ? I18n.t('optionScreen.changeUsernameDialogTitle')
              : I18n.t('optionScreen.setUsernameDialogTitle')}
          </Dialog.Title>
          <Dialog.Input
            placeholder={
              userData && userData.title
                ? I18n.t('optionScreen.changeUsernameDialogHint')
                : I18n.t('optionScreen.setUsernameDialogHint')
            }
            placeholderTextColor="#C7C7CD"
            onChangeText={(text) => {
              if (text !== '') {
                setDisabled(false);
              } else {
                setDisabled(true);
              }
              setUserTitle(text);
            }}
            style={styles.inputField}
          />
          <Dialog.Button label={I18n.t('optionScreen.dialogNegativeBtn')} onPress={handleCancel} />
          <Dialog.Button
            label={I18n.t('optionScreen.dialogPositiveBtn')}
            onPress={handleConfirm}
            disabled={disabled}
            color={disabled ? colors.grey : '#169689'}
          />
        </Dialog.Container>
      </KeyboardAwareScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  profileHeader: {
    alignItems: 'center',
  },
  profileImageContainer: {position: 'relative'},
  username: {
    fontSize: baseFontSize,
    fontWeight: 'bold',
    marginVertical: 15,
    color: darkGray,
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {fontSize: 14, fontWeight: 'bold'},
  subtitle: {fontSize: 14, marginVertical: 10},
  inputField: {
    borderBottomWidth: 1,
    borderBottomColor: gray,
    color: '#000000',
  },
});
