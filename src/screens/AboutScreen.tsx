/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {StyleSheet, View, Text, Linking} from 'react-native';
import {colors} from '../styles/colors';
import {getVersion} from 'react-native-device-info';
import {AboutItem} from '../components/AboutItem';
import {useI18nContext} from '../context/I18nContext';

export default function AboutScreen() {
  const sourceCodeUrl =
    'https://bitbucket.org/steerpath/steerpath-demo-app/src/';
  const steerpathUrl = 'https://steerpath.com';

  const openLink = url => {
    Linking.openURL(url);
  };

  const {I18n} = useI18nContext();

  return (
    <View style={styles.container}>
      <Text style={styles.textView}>
        {I18n.t('aboutScreen.appDescription1')}
      </Text>
      <Text style={styles.textView}>
        {I18n.t('aboutScreen.appDescription2')}
      </Text>
      <AboutItem title={I18n.t('aboutScreen.version')} value={getVersion()} />
      <AboutItem
        title={I18n.t('aboutScreen.sourceCode')}
        value={sourceCodeUrl}
        onPress={() => {
          openLink(sourceCodeUrl);
        }}
      />
      <AboutItem
        title={I18n.t('aboutScreen.website')}
        value={steerpathUrl}
        onPress={() => {
          openLink(steerpathUrl);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  textView: {
    marginBottom: 5,
    fontSize: 14,
  },
  linkStyle: {
    color: colors.link,
    fontWeight: 'bold',
  },
});
