import React from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Platform,
  NativeModules,
} from 'react-native';
import {ListItem} from 'react-native-elements';
import {gray} from '../styles/colors';
import {baseFontSize, smallFontSize} from '../styles/typography';
import {useI18nContext} from '../context/I18nContext';

export default function LanguageScreen({navigation}) {
  const {setLocale, I18n} = useI18nContext();

  function hanldeChangeLocale(locale: string) {
    // Send event to native side, so that map content can be translated
    if (Platform.OS === 'android') {
      const {RNLocaleManager} = NativeModules;
      RNLocaleManager.changeLocale(locale);
    }
    setLocale(locale);
    navigation.pop();
  }

  const currentLocale = I18n.currentLocale();

  return (
    <ScrollView>
      <ListItem
        title="English"
        Component={TouchableOpacity}
        titleStyle={styles.title}
        bottomDivider
        onPress={() => hanldeChangeLocale('en')}
        checkmark={currentLocale === 'en'}
      />
      <ListItem
        title="Suomi"
        subtitle="Finnish"
        titleStyle={styles.title}
        subtitleStyle={styles.subtitle}
        bottomDivider
        checkmark={currentLocale === 'fi'}
        onPress={() => hanldeChangeLocale('fi')}
        Component={TouchableOpacity}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: baseFontSize,
  },
  subtitle: {
    color: gray,
    marginTop: 5,
    fontSize: smallFontSize,
  },
});
