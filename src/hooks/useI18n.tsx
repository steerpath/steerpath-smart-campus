/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as RNLocalize from 'react-native-localize';
import I18n from 'i18n-js';
import {I18nManager} from 'react-native';
import {useEffect, useState} from 'react';
import {isEmpty} from 'lodash';
import en from '../utils/localize/locales/en';
import fi from '../utils/localize/locales/fi';
import {getData, storeData} from '../utils/InternalStorage';

async function setI18nConfig() {
  const storedLocale = await getData('locale');
  const locales = RNLocalize.getLocales();

  if (Array.isArray(locales)) {
    I18n.locale = locales[0].languageTag;
  }

  const translationGetters = {
    en,
    fi,
  };

  I18n.fallbacks = true;

  const fallback = {languageTag: 'en', isRTL: false};

  const {languageTag, isRTL} =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;
  I18nManager.forceRTL(isRTL);
  I18n.translations = translationGetters;

  if (storedLocale) {
    I18n.locale = storedLocale;
  } else {
    I18n.locale = languageTag;
  }
}

export default function useI18n() {
  const [refresh, setRefresh] = useState(false);

  const translationsWereLoaded = () => !isEmpty(I18n.translations);

  useEffect(() => {
    setI18nConfig().finally(() => {
      setRefresh(n => !n);
    });
  }, []);

  const handleLocalizationChange = () => {
    setRefresh(n => !n);
  };

  useEffect(() => {
    RNLocalize.addEventListener('change', handleLocalizationChange);
    // …later (ex: component unmount)
    return () =>
      RNLocalize.removeEventListener('change', handleLocalizationChange);
  }, []);

  async function setLocale(locale: string) {
    I18n.locale = locale;
    await storeData('locale', locale);
    setRefresh(n => !n);
  }

  return {I18n, setLocale, translationsWereLoaded};
}
