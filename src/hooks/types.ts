export interface LiveConfig {
  receive: {
    showsThisDevice: boolean;
    groups: string[];
  };
  transmit: {
    id: string;
    password: string;
    title: string;
    groups: string[];
    geofences: {
      neutral: string[];
      forbidden: string[];
      allowed: string[];
    };
  };
}
export interface UserState {
  userId: string;
  password: string;
  title: string;
  userStatus?: string;
  shareLocation: boolean;
}
