/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect, useState} from 'react';
import {storeData, getData, clearData} from '../utils/InternalStorage';
import RNFS from 'react-native-fs';
import {SmartMapManager} from 'react-native-steerpath-smart-map';
import {LiveConfig, UserState} from './types';
import {Platform} from 'react-native';

const baseurl =
  'https://2jcw71uk36.execute-api.eu-west-1.amazonaws.com/prod/configs?code=';

const CONFIG_FILE_PATH = RNFS.DocumentDirectoryPath + '/steerpath_config.json';

export function useSteerpath() {
  const [initializing, setInitializing] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [errorMsg, setErrorMsg] = useState<string>();
  const [sdkReady, setSdkReady] = useState(false);
  const [userData, setUserData] = useState<UserState>();
  const [liveConfig, setLiveConfig] = useState<LiveConfig>();
  const [liveStarted, setLiveStarted] = useState(false);
  const [shouldStartLive, setShouldStartLive] = useState(false);
  const [usesLive, setUsesLive] = useState(false);

  useEffect(() => {
    getStoredData();
  }, []);

  // Fetches venue data from backend
  async function fetchVenueData(venueCode: string) {
    setIsLoading(true);
    const url = baseurl + venueCode;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        // If config file already exists, remove it first to avoid any conflicts between config files.
        if (RNFS.exists(CONFIG_FILE_PATH)) {
          removeConfigFile();
        }

        const venueData = responseJson.item;

        if (venueData) {
          const smartApiKey = venueData.smartAPIKey;
          storeData('apiKey', smartApiKey);

          // Write file to a path
          RNFS.writeFile(CONFIG_FILE_PATH, venueData.config, 'utf8')
            .then(() => {
              startSteerpath(smartApiKey);
            })
            .catch((err) => {
              console.log(err.message);
            });

          if (venueData.liveConfig) {
            let liveConfig: LiveConfig = venueData.liveConfig;
            setUsesLive(true);

            // Live must be started immediately for iOS after starting SmartSDK,
            // even though location sharing is not enabled. Otherwise, no live
            // room statuses are shown.
            if (Platform.OS === 'ios' && liveConfig) {
              SmartMapManager.setLiveConfig({
                receive: liveConfig.receive,
              });
            }

            // Random string as user id
            const userId =
              userData && userData.userId ? userData.userId : generateUserId();
            liveConfig.transmit.id = userId;

            // this ensures that the password is also always the same.
            const password =
              userData && userData.password
                ? userData.password
                : liveConfig.transmit.password;

            liveConfig.transmit.password = password;

            // store user data
            const user: UserState = {
              title: userData && userData.title ? userData.title : '',
              userId: userId,
              password: password,
              shareLocation: false,
            };

            updateUserData(user);
            setLiveConfig(liveConfig);
            storeData('liveConfig', liveConfig);
          } else {
            setUsesLive(false);
          }
        } else if (responseJson.err) {
          let errorMsg = responseJson.err;
          if (errorMsg === 'Not Found') {
            errorMsg = 'Venue not found with given code.';
          }
          setErrorMsg(errorMsg);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        let errorMsg;
        if (error && error.message) {
          errorMsg = error.message;
        }
        setIsLoading(false);
        setErrorMsg(errorMsg);
      });
  }

  // Get previously used venue data from local storage
  async function getStoredData() {
    const apiKey = await getData('apiKey');
    if (apiKey) {
      startSteerpath(apiKey);
      const config: LiveConfig = await getData('liveConfig');

      if (config) {
        setLiveConfig(config);
        setUsesLive(true);
        // Live must be started immediately for iOS after starting SmartSDK,
        // even though location sharing is not enabled. Otherwise, no live
        // room statuses are shown.
        if (Platform.OS === 'ios' && config) {
          SmartMapManager.setLiveConfig({
            receive: config.receive,
          });
        }
      }

      const storedUser: UserState = await getData('user');
      if (storedUser) {
        updateUserData(storedUser);
      }

      if (storedUser.shareLocation) {
        shareLocation(true);
      }
    } else {
      setInitializing(false);
    }
  }

  // Start Steerpath Smart SDK
  function startSteerpath(smartApiKey) {
    SmartMapManager.startWithConfig({
      apiKey: smartApiKey,
      configFilePath: CONFIG_FILE_PATH,
    });
    setSdkReady(true);
    setIsLoading(false);
    setInitializing(false);
  }

  // Enable or disable location sharing (Steerpath Live)
  function shareLocation(enabled: boolean, title?: string) {
    if (enabled) {
      if (liveConfig) {
        // update liveconfig with new title if it is changed
        if (title && title !== liveConfig.transmit.title) {
          const newConfig = {
            receive: liveConfig.receive,
            transmit: {
              id: liveConfig.transmit.id,
              password: liveConfig.transmit.password,
              title: title,
              groups: liveConfig.transmit.groups,
              geofences: liveConfig.transmit.geofences,
            },
          };

          setLiveConfig(newConfig);
          storeData('liveConfig', newConfig);
          SmartMapManager.setLiveConfig(newConfig);
        } else {
          SmartMapManager.setLiveConfig(liveConfig);
        }

        setLiveStarted(true);
      } else {
        setShouldStartLive(true);
      }
    } else {
      // Stops the live service
      if (liveStarted) {
        SmartMapManager.setLiveConfig((null as unknown) as Record<string, any>);
        setLiveStarted(false);
      }
    }
  }

  // Try to start location sharing again, if it has failed previously
  useEffect(() => {
    if (shouldStartLive && liveConfig) {
      setShouldStartLive(false);
      shareLocation(true);
    }
  }, [shouldStartLive, liveConfig]);

  function removeConfigFile() {
    RNFS.unlink(CONFIG_FILE_PATH)
      .then(() => {
        console.log('FILE DELETED');
      })
      // `unlink` will throw an error, if the item to unlink does not exist
      .catch((err) => {
        console.log(err.message);
      });
  }

  // Clear configurations from async storage
  function clearSavedData() {
    if (usesLive) {
      shareLocation(false);
    }
    removeConfigFile();
    clearData('apiKey');
    clearData('liveConfig');
    setSdkReady(false);
  }

  const generateUserId = (
    passwordLength = 12,
    useUpperCase = true,
    useNumbers = true,
    useSpecialChars = true,
  ) => {
    const chars = 'abcdefghijklmnopqrstuvwxyz';
    const numberChars = '0123456789';
    const specialChars = '!"£$%^&*()';

    const usableChars =
      chars +
      (useUpperCase ? chars.toUpperCase() : '') +
      (useNumbers ? numberChars : '') +
      (useSpecialChars ? specialChars : '');

    let generatedUserId = '';

    let i;
    for (i = 0; i <= passwordLength; i++) {
      generatedUserId +=
        usableChars[Math.floor(Math.random() * usableChars.length)];
    }

    return generatedUserId;
  };

  useEffect(() => {
    if (userData && userData.shareLocation && liveConfig) {
    }
  }, [userData, liveConfig]);

  function updateUserData(data: UserState) {
    setUserData(data);
    storeData('user', data);
  }

  return {
    initializing,
    isLoading,
    errorMsg,
    setErrorMsg,
    fetchVenueData,
    sdkReady,
    clearSavedData,
    shareLocation,
    userData,
    updateUserData,
    usesLive,
  };
}
