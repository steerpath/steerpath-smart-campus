import * as SentryReactNative from '@sentry/react-native';

export function sentryInit() {
  if (!__DEV__) {
    SentryReactNative.init({
      dsn:
        'https://9c63f88628ad436cb6261d7af33ca66f@o395500.ingest.sentry.io/5253530',
    });
  }
}
