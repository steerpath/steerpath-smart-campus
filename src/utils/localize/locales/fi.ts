export default {
  appTabs: {
    map: 'Kartta',
    option: 'Valikko',
  },
  venueCodeScreen: {
    inputTitle: 'Rakennus koodi',
    inputBtn: 'Avaa rakennus',
    description:
      'Käyttääksesi Steerpathin sisätilapaikannusta ja reititystä, ole hyvä ja syötä sinun rakennus koodisi. Voit kokeilla Steerpathin toimistoa käyttämällä koodia',
  },
  optionScreen: {
    title: 'Valikko',
    profileCard: {
      visibility: 'Näkyy muille',
    },
    aboutBtnTitle: 'Tiedot',
    aboutBtnDescription: 'Tietoja sovelluksesta',
    venueCodeBtnTitle: 'Rakennus koodi',
    venueCodeBtnDescription:
      'Palaa takaisin alkunäkymään ja aseta uusi rakennus koodi.',
    setUsernameDialogTitle: 'Käyttäjänimi pakollinen',
    setUsernameDialogHint: 'Kirjoita käyttäjänimi',
    changeUsernameDialogTitle: 'Vaihda käyttäjänimi',
    changeUsernameDialogHint: 'Kirjoita uusi käyttäjänimi',
    dialogNegativeBtn: 'Peru',
    dialogPositiveBtn: 'Vahvista',
    preferences: 'Asetukset',
    changeLanguageBtnTitle: 'Kielet',
    changeLanguageBtnDescription: 'Vaihda kieli',
  },
  aboutScreen: {
    title: 'Tiedot',
    appDescription1:
      'Tätä sovellusta voi käyttää Steerpathin tarjoaman rakennus koodin kanssa. Sovellusta voidaan käyttää karttojen katselemiseen, sisätilapaikannukseen ja oman sijainnin jakamiseen muille käyttäjille.',
    appDescription2:
      'Sovellus toimii myös esimerkkinä siitä miten sovellus luodaan Steepath Smart SDK:n päälle react-nativella.',
    version: 'Versio:',
    sourceCode: 'Lähdekoodi:',
    website: 'Nettisivusto:',
  },
};
