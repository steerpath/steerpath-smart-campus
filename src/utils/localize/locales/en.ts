export default {
  appTabs: {
    map: 'Map',
    option: 'Option',
  },
  venueCodeScreen: {
    inputTitle: 'Venue Code',
    inputBtn: 'Open venue',
    description:
      "To use Steerpath indoor positioning and wayfinding, please enter your venue's code. You can try Steerpath's office by using code",
  },
  optionScreen: {
    title: 'Option',
    profileCard: {
      visibility: 'Visible to others',
    },
    aboutBtnTitle: 'About',
    aboutBtnDescription: 'Information about the application',
    venueCodeBtnTitle: 'Venue Code',
    venueCodeBtnDescription: 'Navigate back to start and set a new venue code.',
    setUsernameDialogTitle: 'Username required',
    setUsernameDialogHint: 'Type username',
    changeUsernameDialogTitle: 'Change username',
    changeUsernameDialogHint: 'Type new username',
    dialogNegativeBtn: 'Cancel',
    dialogPositiveBtn: 'Confirm',
    preferences: 'Settings',
    changeLanguageBtn: 'Languages',
    changeLanguageBtnDescription: 'Change language',
  },
  aboutScreen: {
    title: 'About',
    appDescription1:
      'You can use this app with the venue code provided by Steerpath. The application can be used to view maps, use indoor positioning in your venue and to share your location to other users.',
    appDescription2:
      'Applications also works as an example of how to build an application on top of Steerpath Smart SDK by using react-native.',
    version: 'Version:',
    sourceCode: 'Source Code:',
    website: 'Website:',
  },
};
