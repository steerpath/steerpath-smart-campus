/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useEffect} from 'react';

import SplashScreen from 'react-native-splash-screen';

import {SteerpathProvider} from './context/SteerpathContext';

import {RootNavigator} from './navigator/RootNavigator';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {sentryInit} from './sentry/initSentry';
import {I18nContext} from './context/I18nContext';
import useI18n from './hooks/useI18n';

sentryInit();

export default function App() {
  const {I18n, setLocale, translationsWereLoaded} = useI18n();

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <SafeAreaProvider>
      <I18nContext.Provider value={{I18n, setLocale, translationsWereLoaded}}>
        <SteerpathProvider>
          <RootNavigator />
        </SteerpathProvider>
      </I18nContext.Provider>
    </SafeAreaProvider>
  );
}
