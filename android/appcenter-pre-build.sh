#!/usr/bin/env bash


# We need to manually increment android build id so that sentry can match with the source map
if [ "$APPCENTER_BUILD_ID" ]; then
  echo "Updating android build id"

  # Updating ids

  buildGradle=./android/app/build.gradle

  sed -i '' "s/versionCode 1/versionCode $APPCENTER_BUILD_ID/g" $buildGradle

  # Print out file for reference
  cat $buildGradle

  echo "Updated build id"
fi