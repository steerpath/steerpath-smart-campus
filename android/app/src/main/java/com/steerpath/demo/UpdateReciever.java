package com.steerpath.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.io.File;

public class UpdateReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (BuildConfig.VERSION_CODE == 17) {
            clearOldAppData(context);
        }
    }

    private void clearOldAppData(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            try {
                context.deleteSharedPreferences("demo");
            } catch (Exception e) {
                Log.e("Error", "no preferences found");
            }
        }


        // Remove old databases
        try {
            context.deleteDatabase("steerpathdemo.DB");
        } catch (Exception e) {
            Log.e("Error", "Database not found, can't remove it");
        }

        deleteCache(context);
    }


    private void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
