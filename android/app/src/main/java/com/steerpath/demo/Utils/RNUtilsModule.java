package com.steerpath.demo.Utils;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nonnull;

public class RNUtilsModule extends ReactContextBaseJavaModule {

    private ReactContext context;

    public RNUtilsModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        this.context = reactContext;
    }

    @NonNull
    @Override
    public String getName() {
        return "RNUtilsModule";
    }

    @ReactMethod
    public void moveTaskToBack(Callback callback) {
        Activity activity = getCurrentActivity();
        if (activity != null) {
            boolean wasMoved = activity.moveTaskToBack(true);
            if (callback != null) {
                callback.invoke(wasMoved);
            }
        }
    }
}
