# Steerpath Smart Campus


## Contributing

Clone this repository and create a new branch locally. Use `feature/` or `fix/` prefixes in branch naming i.e. `feature/my-new-feature`. Always use clear log messages for you commits. One line messages are fine for small changes, but bigger changes should have a briref summary (title) and a body describing the changes.

```bash
$ git commit -m "A brief summary of the commit
>
> A paragraph describing what changed and its impact."
```

When you are finished with your work, please open a Pull Request to `develop` branch with a clear description of what you've done.

## License

[Apache 2.0](https://choosealicense.com/licenses/apache-2.0/)
